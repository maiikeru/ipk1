##
# Projekt: IPK 1 
# Branch:  Master
# Autor:   Michal Jurca
# Datum:   23.3.2014  
# Změna:   23.3.2014  
# Použití:
#	-překlad     make
#	-smazat      make clean
#	-zabalit     make pack                  
##                      

###############################################################################
#  Parameters
###############################################################################

# Jmeno pro pack
NAME = xjurca07

# Smazaní souborů
RM = rm -f

# Optimalizace
OPT = -g -O3 

# Parametry a compilator pro C++
#CPPFLAGS = g++ -std=c++0x -pedantic -Wall -Werror -pedantic-errors $(OPT)

CPPFLAGS = g++ -std=c++98 

# Zdrojový soubor
SRCS =  $(wildcard *.cpp)

# Objektový soubor
OBJS = $(SRCS:.cpp=.o)

# Spustitelný soubor
FILES = ftpclient 

# Obsah projektu
ALLFILES =  Makefile $(SRCS)

###############################################################################
#  Compilation
###############################################################################

.PHONY: clean pack 

all: $(FILES)


ftpclient: ftpclient.cpp
	$(CPPFLAGS) -o $@ $<	

###############################################################################
#  Cleaning and packing
###############################################################################

clean:
	$(RM) $(OBJS) $(FILES) 

pack:
	zip $(NAME).zip $(ALLFILES)

# End of Makefile
	