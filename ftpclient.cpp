/*
***
* Soubor: ftpclient.cpp
* verze: 0.8
* Změna: 23.3.2014
*
* Projekt: Projekt 1: FTP klient ; VUT FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 10.3.2014
* Kompilator: g++ (Ubuntu/Linaro 4.8.1-10ubuntu9) 4.8.1
*             g++ (GCC) 4.2.1 20070831 patched [FreeBSD]
***
*/

#include <iostream>
//#include <regex>
#include <string>

#include <string.h>
#include <regex.h>

#include <sys/types.h> //socket
#include <sys/socket.h> // socket
#include <netdb.h> // getnamebyhost
#include <unistd.h> // close

#include <sys/time.h>
#include <sys/select.h>

#include <cstdlib>
#include <netinet/in.h>

// nemusi se pouzivat std::
using namespace std;

typedef struct
{
    string source;
    string user;
    string pass;
    string address;
    int port;
    char buff[2048];
} data;

/*
#
*/
void PrintHelp(const char *param);

/*
#
*/
bool IsEreg(const char *sourceRV);

/*
#
*/
void ZpracujVstup(data *point);

/*
#
*/
bool SendLogin(data *point , int soccket);

/*
#
*/
bool SendPass(data *point , int soccket);

/*
#
*/
bool ReceiveMSG(data *point, int soccket);

/*
#
*/
bool SendList(data *point, int soccket);

/*
#
*/
int main(int argc, char const *argv[])
{
    data pointer;

    if (argc != 2)
    {
        cerr << "ARGV: zadany chybne parametry. Pro vice informaci '" << argv[0] << " --help'." << endl;
        return 1;
    }
    else if (strcmp(argv[1], "--help") == 0)
    {
        PrintHelp(argv[0]);
        return 0;
    }
    else
    {
        if (IsEreg(argv[1]) != 1)
        {
            cerr << "Syntax error. Zadany parametr: '" << argv[1] << "' neni validni." << endl;
            return 1;
        }
    }

    // převedení vstupu na string
    int i = 0;
    while (argv[1][i])
    {
        pointer.source += argv[1][i];
        i++;
    }

    //  kontrola jestli na zacatku je ftp//
    if (strncmp(argv[1], "ftp://", 6) == 0)
        pointer.source.erase(0, 6);

    // Zpracovani vstupu na adresu,port, cil pro LIST
    ZpracujVstup(&pointer);

    int sockfd, socckett_second;
    struct sockaddr_in sin;
    struct sockaddr_in otec;
    struct hostent *server;

    // Create socket
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        cerr << "Nelze vytvorit socket" << endl;
        return 1;
    }

    // Configure socket
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(pointer.port);

    server = gethostbyname(pointer.address.c_str());
    if (server == NULL)
    {
        cerr << "Nelze provest preklad adresy do IPv4" << endl;
        close(sockfd);
        return 1;
    }
    memcpy((char *)&sin.sin_addr, (char *)server->h_addr_list[0], server->h_length);

    if (connect(sockfd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        cerr << "Nelze se pripojit" << endl;
        return 1;
    }

    // Prijem zpravy
    if (  ReceiveMSG(&pointer , sockfd) != 0)
        return 1;

    // Poslani loginu
    if (  SendLogin(&pointer , sockfd) != 0)
        return 1;

    if (  ReceiveMSG(&pointer , sockfd) != 0)
        return 1;

    // Poslani hesla
    if (  SendPass(&pointer , sockfd) != 0)
        return 1;

    // tezka inspirace man select 2
    //*******************************************
    fd_set rfds;
    struct timeval tv;
    int retval;

    tv.tv_sec = 3;
    tv.tv_usec = 0;

    while (1)
    {
        FD_ZERO(&rfds);
        FD_SET(STDIN_FILENO, &rfds);
        bzero(pointer.buff, sizeof(pointer.buff));
        if (  ReceiveMSG(&pointer , sockfd) != 0)
            return 1;

        if (strncmp(pointer.buff, "530", 3) == 0)
        {
            cerr << "Error: Login incorrect" << endl;
            close(sockfd);
            return 1;

        }

        retval = select(1, &rfds, NULL, NULL, &tv);

        if (retval == -1)
        {
            cerr << "select" << endl;
            close(sockfd);
            return 1;
        }
        else if (retval)
        {
            if (FD_ISSET(sockfd, &rfds))
                continue;
            else
                break;
        }
        else
        {
            bzero(pointer.buff, sizeof(pointer.buff));
            if (  ReceiveMSG(&pointer , sockfd) != 0)
                return 1;
            break;
        }
    }
    //********************************************************
    if ((send(sockfd, "EPSV\r\n", 6, 0)) == -1)
    {
        cerr << "Poslani serveru EPSV" << endl;
        return 1;
    }
    if (  ReceiveMSG(&pointer , sockfd) != 0)
        return 1;

    i = 3;
    string source;
    while (pointer.buff[i])
    {
        if (pointer.buff[i] >= 48 && pointer.buff[i] <= 57)
        {
            source += pointer.buff[i];
        }
        i++;
    }
    //int num = stoi(source,nullptr, 10);
    int num = atoi(source.c_str());

    // Create socket
    if ((socckett_second = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        cerr << "nelze vytvorit socket" << endl;
        return 1;
    }

    // Configure socket
    memset(&otec, 0, sizeof(otec));
    otec.sin_family = AF_INET;
    otec.sin_addr.s_addr = INADDR_ANY;
    otec.sin_port = htons(num);
    memcpy((char *)&otec.sin_addr, (char *)server->h_addr_list[0], server->h_length);

    if (connect(socckett_second, (struct sockaddr *)&otec, sizeof(otec)) < 0)
    {
        cerr << "Nelze se pripojit" << endl;
        close(sockfd);
        close(socckett_second);
        return 1;
    }

    if (  SendList(&pointer , sockfd) != 0)
    {
        close(socckett_second);
        return 1;
    }

    while (1)
    {
        FD_ZERO(&rfds);
        FD_SET(STDIN_FILENO, &rfds);
        bzero(pointer.buff, sizeof(pointer.buff));
        if (  ReceiveMSG(&pointer , sockfd) != 0)
        {
            close(socckett_second);
            return 1;
        }

        retval = select(1, &rfds, NULL, NULL, &tv);

        if (retval == -1)
        {
            cerr << "select" << endl;
            close(sockfd);
            close(socckett_second);
            return 1;
        }
        else if (retval)
        {
            if (FD_ISSET(sockfd, &rfds))
                continue;
            else
                break;
        }
        else
            break;
    }
    bzero(pointer.buff, sizeof(pointer.buff));

    if (  ReceiveMSG(&pointer , socckett_second) != 0)
    {
        close(sockfd);
        return 1;
    }

    /*Vystup*/
    cout << pointer.buff;

    if ((send(sockfd, "NOOP\r\n", 6, 0)) == -1)
    {
        cerr << "Poslani serveru NOOP" << endl;
        close(socckett_second);
        return 1;
    }

    if (  ReceiveMSG(&pointer , sockfd) != 0)
    {
        close(socckett_second);
        return 1;
    }

    close(sockfd);
    close(socckett_second);

    return 0;
}

void PrintHelp(const char *param)
{
    cout << "Projekt 1: FTP klient\n"
         << "===============================================\n"
         << "Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz\n"
         << "Popis: Program vypisuje obsah vzdalenoho adresare FTP serveru na standartni vystup.\n"
         << "       format adresy: [ftp://[user:password@]]host[:port][/path][/]\n"
         << "Parametry:\n"
         << "\t" << param << " --help      * vytiskne napovedu\n"
         << "\t" << param << " address     * adresa FTP serveru" << endl;
}

bool IsEreg(const char *sourceRV)
{
    regex_t start_state;
    const char *pattern = "^(ftp:\\/\\/)?([a-zA-Z0-9@#&*]+\\:[a-zA-Z0-9@#&*]+\\@)?([a-zA-Z]+(\\.)?)+(\\:[0-9]+)?(\\/[a-zA-Z]+(\\/)?)*(\\/)?$";
    if (regcomp(&start_state, pattern, REG_EXTENDED))
    {
        cerr << "IsEreg: chybny regulerni vyraz: " << pattern << endl;
        return 0;
    }
    int status = regexec(&start_state, sourceRV, (size_t) 0, NULL, 0);
    regfree(&start_state);

    return (status != 0 ? false : true);
}

void ZpracujVstup(data *point)
{
    point->port = 21;
    point->address = point->source;

    char *dup;
    regex_t start_state;
    const char *pattern = "(.*):(.*)@(.*)";
    int status;
    if (0 != (status = regcomp(&start_state, pattern, REG_EXTENDED)))
    {
        cerr << "chyba pri regulernim vyrazu" << endl;
        exit(EXIT_FAILURE);
    }

    //regex heslo  ("(.*):(.*)@(.*)");

    if (0 == (status = regexec(&start_state, point->source.c_str(), (size_t) 0, NULL, 0)))
    {
        // ziskani jmena
        dup = strdup(point->source.c_str());
        point->user = strtok(dup, ":");
        point->source.erase(0, point->user.length() + 1);

        // ziskani hesla
        dup = strdup(point->source.c_str());
        point->pass = strtok(dup, "@");
        point->source.erase(0, point->pass.length() + 1);
        point->address = point->source;
    }

    //port
    //regex port  ("(.*):(.*)");
    pattern = "(.*):(.*)";

    if (0 != (status = regcomp(&start_state, pattern, REG_EXTENDED)))
    {
        cerr << "chyba pri regulernim vyrazu" << endl;
        exit(EXIT_FAILURE);
    }

    if (0 == (status = regexec(&start_state, point->source.c_str(), (size_t) 0, NULL, 0)))
    {
        dup = strdup(point->source.c_str());
        point->address = strtok(dup, ":");
        point->source.erase(0, point->address.length() + 1);
        //point->port = stoi(point->source,NULL, 10);
        point->port = atoi(point->source.c_str());
        int j = 0;
        while (point->source[j] <= 57)
            j++;

        point->source.erase(0, j - 1);
        free(dup);
    }
    else
    {
        // regex cil  ("(.*)/(.*)");
        pattern = "(.*)/(.*)";

        if (0 != (status = regcomp(&start_state, pattern, REG_EXTENDED)))
        {
            cerr << "chyba pri regulernim vyrazu" << endl;
            exit(EXIT_FAILURE);
        }

        if (0 == (status = regexec(&start_state, point->source.c_str(), (size_t) 0, NULL, 0)))
        {
            dup = strdup(point->source.c_str());
            point->address = strtok(dup, "/");
            point->source.erase(0, point->address.length());
            free(dup);
        }
        else
        {
            point->source.erase();
        }
    }
    regfree(&start_state);
}

bool SendLogin(data *point , int soccket)
{
    if (point->user.size() > 0)
    {
        string retezec ("USER " + point->user + "\r\n");
        if ((send(soccket, retezec.c_str(), retezec.size(), 0)) == -1)
        {
            cerr << "Nelze poslat login uzivatele: " << point->user << endl;
            close(soccket);
            return 1;
        }
    }
    else
    {
        if ((send(soccket, "USER anonymous\r\n", 16, 0)) == -1)
        {
            cerr << "Nelze poslat login anonymous" << endl;
            close(soccket);
            return 1;
        }
    }
    return 0;
}

bool SendPass(data *point , int soccket)
{
    if (point->pass.size() > 0)
    {
        string retezec ("PASS " + point->pass + "\r\n");
        if ((send(soccket, retezec.c_str(), retezec.size(), 0)) == -1)
        {
            cerr << "Nelze poslat password uzivatele: " << point->pass << endl;
            close(soccket);
            return 1;
        }
    }
    else
    {
        if ((send(soccket, "PASS anonymous\r\n", 16, 0)) == -1)
        {
            cerr << "Nelze poslat password anonymous" << endl;
            close(soccket);
            return 1;
        }
    }
    return 0;
}

bool ReceiveMSG(data *point, int soccket)
{
    int bytes;
    if ((bytes = recv(soccket, point->buff, sizeof(point->buff), 0)) == -1)
    {
        cerr << "Error: nelze prijmout zpravu" << endl;
        close(soccket);
        return 1;
    }
    point->buff[bytes] = '\0';
    return 0;
}

bool SendList(data *point, int soccket)
{
    if (point->source.size() > 0)
    {
        string retezec ("LIST " + point->source + "\r\n");
        if ((send(soccket, retezec.c_str(), retezec.size(), 0)) == -1)
        {
            cerr << "Nelze poslat LIST " << point->source << endl;
            return 1;
        }
    }
    else
    {
        if ((send(soccket, "LIST\r\n", 6, 0)) == -1)
        {
            cerr << "Nelze poslat prikaz LIST" << endl;
            close(soccket);
            return 1;
        }
    }
    return 0;
}
